
$(document).ready(function() {
    //toggle `popup` / `inline` mode
    $('.editBtn').click(function(){
        $.fn.editable.defaults.mode = 'inline';
        //make status editable
        $('.edit').editable({
            type: 'textarea',
            placement: 'right',
            send: 'auto',
            toggle:'dblclick'
        });
    });

    var count = $('.counter').text();
    var arraycount = count.length;
    var removeLastComma = count.slice(0 , arraycount-1);
    var arrayCount =  removeLastComma.split(',');
    var i;
    var a;
    var aCount = "";
    var sideMenu = "";

    var apiNam = $('.nameOfApi').text();

    var theSplit = apiNam.split(' ');

    for(a = 0; a < theSplit.length; a++){
        sideMenu += "<a href ='#"  + theSplit[a] + "'>"  + theSplit[a] + "</a></br>"
    }

    document.getElementById('sideMenu').innerHTML = sideMenu;

    for(i = 0; i < arrayCount.length; i++) {

     aCount += arrayCount[i];

    }

    $("button").click(function(){
     var correctModal = ($(this).attr('id'));
        $("#areYouSure" + correctModal).modal();
    });

    $(".showHide").click(function(){
        var correctNumber = ($(this).attr('id'));
        $("#requestBody" + correctNumber).toggle('slow');
    });

    $(".showHideSch").click(function(){
        var correctNumberSch = ($(this).attr('id'));
        $("#requestSchema" + correctNumberSch).toggle('slow');
    });

    $(".showHideresBody").click(function(){
        var correctNumberRes = ($(this).attr('id'));
        $("#responseBody" + correctNumberRes).toggle('slow');
    });

    $(".showHideresSch").click(function(){
        var correctNumberResSch = ($(this).attr('id'));
        $("#responseSchema" + correctNumberResSch).toggle('slow');
    });


});
