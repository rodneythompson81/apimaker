<?php
$servername = "216.189.153.161";
$username = "apidoc";
$password = "AP1D0Cum3nt";
$dbname = "apidoc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// sql to create table
$sql = "CREATE TABLE apiDoc (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30) NOT NULL,
callType VARCHAR(30) NOT NULL,
call VARCHAR(30) NOT NULL,
description VARCHAR(200) NOT NULL,
requestBody VARCHAR(30) NOT NULL,
requestSchema VARCHAR(30) NOT NULL,
responseBody VARCHAR(30) NOT NULL,
responseSchema VARCHAR(30) NOT NULL,
reg_date TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
    echo "Table apiDoc created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>
