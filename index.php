<!DOCTYPE html>
<html>
<head>
    <script type='text/javascript' src='knockout-3.3.0.js'></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="api.css">
    <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <link href="css/bootstrap-editable.css" rel="stylesheet">
    <script src="js/bootstrap-editable.js"></script>
    <script src="lodash.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script>


    function contDelete(id){
       $.post("deleteApi.php",
                    {
                    id: id
                          },
                    function(data,status){
                        console.log("Data: " + data + "\nStatus: " + status);
                        location.reload();
                    });
        }

    function navTo(name){
    console.log(name);
        //ar name = $('#' + name);
        //var y =  name.offset();
        //name.scrollTo(y.top);

    }
    </script>
    <?php
    $servername = "";
    $username = "apidoc";
    $password = "";
    $dbname = "";
    ?>
    <script src='apiDoc.js'></script>
</head>
<body>
<div id="fullPage">
 <div class="content">
        <form>
            <input type="text" data-bind="value:moduleTitle"/>
            <button data-bind = "click: showForm">ADD API</button>
            <button data-bind = "click: deleteForm">Delete Form</button>
        </form></br>
  <div class="container">
    <div data-bind="foreach:apiForm">
        <div id="display" data-bind="html:form">
        </div></br>
    </div>
  </div>
 </div>
 <div style="width:1150px; margin:auto">
 <div class="clickSide" id="sideMenu"  style="width: 125px; float:right; font-size:23px; position: fixed;">
 </div>

<?php
 // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT `id`, `name`, `callType`, `call`, `description`, `requestBody`, `requestSchema`, `responseBody`, `responseSchema`  FROM API";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $id = $row["id"];
            //$moduleTitle =
            $increment = 0;
            $nameTitle = $row["name"];
            $nameTitleSM = $row["name"];
            $callType = $row["callType"];
            $call = $row["call"];
            $description = $row["description"];
            $requestBody = "<pre><code>".$row["requestBody"]."</code></pre>";
            $requestSchema = "<pre><code>".$row["requestSchema"]."</code></pre>";
            $responseBody = "<pre><code>".$row["responseBody"]."</code></pre>";
            $responseSchema = "<pre><code>".$row["responseSchema"]."</code></pre>";

            if($callType == "GET"){
                     $color = 'blue';
                     $bgColor = '#ADD8E6';
                } elseif($callType == "POST"){
                     $color = 'green';
                     $bgColor = '#66CDAA';
                } elseif($callType == "DELETE"){
                     $color = 'red';
                     $bgColor = '#FFE4E1';
                } elseif($callType == "PUT"){
                     $color = 'orange';
                     $bgColor = '#F0E68C';
                }
        echo "
               <div style='width:900px; margin:auto; padding:10px; background-color:#E0E0E0; border-radius: 25px; '>
                 <div style='display:none' class='nameOfApi'>".$nameTitle." </div>
                   <div style='padding:5px' id='".$nameTitle."' ><h2 style='font-size:56px'>".$nameTitle."</h2><h4 style='float:left; position:relative;top:-5px'></h4>
                   <button style='border: 2px solid #a1a1a1;' type='button' class='btn btn-xs' id='".$id."'>Delete ". $nameTitle."</button></br></br>
                     <!-- Modal -->
                     <div class='modal fade' id='areYouSure".$id."' role='dialog'>
                       <div class='modal-dialog modal-sm'>
                         <div class='modal-content'>
                           <div class='modal-header'>
                             <button type='button' class='close' data-dismiss='modal'>&times;</button>
                             <h4 class='modal-title'>".$nameTitle."</h4>
                           </div>
                           <div class='modal-body'>
                             <p>Are You Sure You Want To Delete ".$nameTitle." API</p>
                           </div>
                           <div class='modal-footer'>
                             <button type='button' class='btn btn-default' onclick=\"contDelete('".$id."')\" data-dismiss='modal'>Yes</button>
                             <button type='button' class='btn btn-default' data-dismiss='modal'>No</button>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                     <div style='width:100%; clear:left; padding:5px; background-color:".$bgColor."'>
                       <div style='float:left; padding:0px 7px; border-radius: 10px; background-color:".$color."'>
<div class='edit' style='color:white;' href='#' id='callType' data-name='callType' data-type='textarea' data-pk='".$id."' data-url='updateApi.php'><h4>".$callType."</h4></div>
                       </div>
                       <div style='margin:0 80px'>
                         <a style='color:white;' href='#".$nameTitle."' id='call' data-name='call' data-type='textarea' data-pk='".$id."' data-url='updateApi.php'><h4>".$call."</h4></a>
                       </div>
                     </div>

                         <div id='apiUpdate' style='padding:5px; background-color: #fff'>
                        <div class='edit' id='description' data-name='description' data-type='textarea' data-pk='".$id."' data-url='updateApi.php'>
                              <p>".$description."</p>
                            </div>
                         </div>
                            <div style='padding:5px'>
                               <h2>Request</h2>
                            </div>
                             <div style='display:none' class='counter' id='".$id."'>".$id."</div>


                               <div class='reqBody' style='padding:5px; margin:auto'>

                                     <div style='display:none' class='showHideButtonReqBody".$id."'>reqBody".$id."</div>
                                        <h4>Body</h4></br>

                                   <div> <!---style='display:none' class='toggleTextReqBody'-->
                                   <div class='editBtn'><button style='border: 2px solid #a1a1a1;' type='button' class='btn btn-xs'>Edit Mode</button></div>
                                   <div class='showHide' id='".$id."'>Show/Hide</div>
                                   <div id='requestBody".$id."'>
    <div class='edit'  data-name='requestBody' data-type='textarea' data-pk='".$id."' data-url='updateApi.php'><p>".$requestBody."</p></div></div>
                                   </div>
                               </div>
                                      <div class='reqSchema' style='padding:5px; margin:auto'>

                                          <div style='display:none' class='showHideReqSchema'>reqSchema".$id."</div>
                                              <h4>Schema</h4></br>
                                          <div> <!---style='display:none' class='toggleTextReqSchema".$id."'-->
                                          <div class='editBtn'><button style='border: 2px solid #a1a1a1;' type='button' class='btn btn-xs'>Edit Mode</button></div>
                                          <div class='showHideSch' id='".$id."'>Show/Hide</div>
                                          <div id='requestSchema".$id."'>
<div class='edit' id='requestSchema".$id."' data-name='requestSchema' data-type='textarea' data-pk='".$id."' data-url='updateApi.php'><p>".$requestSchema."</p></div></div>
                                          </div>
                                      </div>
                                            
                                                <div style='padding:5px;'>
                                                   <h2>Response</h2>
                                                </div>
                                                     <div class='resBody' style='padding:5px; margin:auto'>

                                                          <div style='display:none' class='showHideButtonResBody'>resBody".$id."</div>
                                                               <h4>Body</h4></br>
                                                                  <div> <!---style='display:none' class='toggleTextResBody".$id."'-->
                                                                  <div class='editBtn'><button style='border: 2px solid #a1a1a1;' type='button' class='btn btn-xs'>Edit Mode</button></div>
                                                                  <div class='showHideresBody' id='".$id."'>Show/Hide</div>
                                                                  <div id='responseBody".$id."'>
    <div class='edit'  data-name='responseBody".$id."' data-type='textarea' data-pk='".$id."' data-url='updateApi.php'><p>".$responseBody."</p></div></div>
                                                                  </div>
                                                     </div>
                                                    <div class='reqSchema' style='padding:5px; margin:auto'>

                                                         <div style='display:none' class='showHideButtonResSchema'>resSchema".$id."</div>

                                                            <h4>Schema</h4></br>
                                                        <div> <!---style='display:none' class='toggleTextResSchema".$id."'-->
                                                        <div class='editBtn'><button style='border: 2px solid #a1a1a1;' type='button' class='btn btn-xs'>Edit Mode</button></div>
                                                        <div class='showHideresSch' id='".$id."'>Show/Hide</div>
                                                        <div id='responseSchema".$id."'>
    <div class='edit' data-name='responseSchema' data-type='textarea' data-pk='".$id."' data-url='updateApi.php'><p>".$responseSchema."</p></div></div>
                                                        </div>
                                                     </div>
                                            
               </div>

            <hr>";

        }
    } else {
        echo "0 results";
    }
    $conn->close();
    ?>
    </div>
<script>
    var BetterListModel = function () {

        this.moduleTitle = ko.observable('');

        this.sideMenu = ko.observableArray('');

        this.apiForm = ko.observableArray([]);

        this.showForm = function () {
            this.apiForm.push({ form: "<form role='form' action='saveApi.php' method='POST'><div class='form-group'><label for='nameTitle'>Name:</label><input type='text' class='form-control' name='nameTitle' id='nameTitle' /></div><div class='form-group'><label for='callType'>CALL TYPE</label><select  class='form-control' name='callType' id='callType'><option value='GET'>GET</option><option value='POST'>POST</option><option value='DELETE'>DELETE</option><option value='PUT'>PUT</option><select/></div><div class='form-group'><label for='call'>CALL</label><input type='text' class='form-control' name='call' id='call'/></div><div class='form-group'><label for='description'>DESCRIPTION</label><input class='form-control' type='text' name='description' id='description'/></div><div class='form-group'><label for='requestBody'>REQUEST BODY</label><input type='text' class='form-control' name='requestBody' id='requestBody'/></div><div class='form-group'><label for='requestSchema'>REQUEST SCHEMA</label><input type='text' class='form-control' name='requestSchema' id='requestSchema'/></div><div class='form-group'><label for='responseBody'>RESPONSE BODY</label><input type='text' class='form-control' name='responseBody' id='responseBody'/></div><div class='form-group'><label for='responseSchema'>RESPONSE SCHEMA</label><input type='text'  class='form-control' name='responseSchema' id='responseSchema'/></div><button type='submit'>Add</button></form><br><br>"});
        }

        this.deleteForm = function () {
            this.apiForm.pop();
        }
    }
    ko.applyBindings(new BetterListModel());
</script>
</div>
</body>
</html>